# Build Docker Image

<h4> Known issues with multi-stage builds for containerized Python applications

Currently, there’s a known problem with pre-stages not being cached. The easiest way to solve this is by using BuildKit and adding the argument `BUILDKIT_INLINE_CACHE=1` to the build process.

We’ll build normally the first time, but subsequent builds would use the following command:


`
    export DOCKER_BUILDKIT=1
    docker build -t flask-application --cache-from flask-application --build-arg BUILDKIT_INLINE_CACHE=1 .
`


You will need the environment variable `DOCKER_BUILDKIT=1` so Docker knows to use BuildKit for the build process. It can also be enabled by adding the following settings in the Docker software configuration file at `/etc/docker/daemon.json` (then the environment variable wouldn’t be needed):


`{ "features": { "buildkit": true } }`